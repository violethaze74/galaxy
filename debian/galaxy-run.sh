#!/bin/bash

cd /usr/share/galaxy

./run.sh --skip-venv --skip-eggs --skip-wheels $@
